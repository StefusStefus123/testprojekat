# from re import I 
import sys
# I znaci Ignorances
from PySide2.QtWidgets import QApplication
from gui.main_window import MainWindow


# Pravljenje novog prozora (Pushovati na git kao zaseban zadatak kreiranje MainWindowa)
if __name__ == "__main__":
    app = QApplication([])
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())

